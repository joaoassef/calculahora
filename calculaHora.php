<?php
//CALCULA INTERVALO DE TEMPO EM HORA
                        function calculaHora($h1, $h2){
                            $entrada     = $h1;
                            $saida       = $h2;
                            $hora1       = explode(":",$entrada);
                            $hora2       = explode(":",$saida);
                            $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
                            $acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
                            $resultado   = $acumulador2 - $acumulador1;
                            $hora_ponto  = floor($resultado / 3600);
                            $resultado   = $resultado - ($hora_ponto * 3600);
                            $min_ponto   = floor($resultado / 60);
                            $resultado   = $resultado - ($min_ponto * 60);
                            $secs_ponto  = $resultado;
                            if(strlen($hora_ponto) <= 1){
                                $hora_ponto = "0".$hora_ponto;
                            }
                            if(strlen($min_ponto) <= 1){
                                $min_ponto = "0".$min_ponto;
                            }
                            if(strlen($secs_ponto) <= 1){
                                $secs_ponto = "0".$secs_ponto;
                            }
                            $tempo = $hora_ponto.":".$min_ponto.":".$secs_ponto;
                            return $tempo;
                        }
